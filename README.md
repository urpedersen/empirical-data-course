# Empirical Data Course

An introductory course to empirical data.

[Try notebooks without having python and jupyter installed (link to myBinder.org). It might take a while to open.](https://mybinder.org/v2/gl/natural-sciences-ruc%2Fempirical-data-course/master)

## Get started on your own
1.  [Get started Jupyter Notebook and Python (markdown)](./documents/get_python.md)
3.  [List of online resources](./documents/Resources.md)

## Compendium
[The compendium as a PDF file (pdf)](compendium_emprircal_data.pdf)

### Additional examples
Below is a collection of notebook examples to give inspiration:
* [Simulation of a spring](./examples/BK2fysik_gpu_output.ipynb)
* [Live earthquake data](./examples/Earthquake data.ipynb)
