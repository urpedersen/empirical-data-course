mu = 30             # Theoretical mean
sigma = 10          # Theoretical standard deviation
n = 4               # Samples in each meta-experiment
experiments = 1000  # Number of meta-experiments

import pandas as pd
from numpy.random import randn  # Draw values from normal distribution
df = pd.DataFrame(sigma*randn(experiments, n) + mu)
df['sample mean'] = df.mean(axis='columns')

import matplotlib.pyplot as plt
from numpy import arange
df['sample mean'].hist(bins=arange(10, 50, 2))
plt.xlim(10, 50)
plt.xlabel(f'Mean in sample with n={n}')
