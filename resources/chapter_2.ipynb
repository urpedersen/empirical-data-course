{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Technical line for inline figures in notebook below*:\n",
    "# = 'pdf': Makes pretty figures for pdf document.\n",
    "# = 'svg': Makes pretty figures for interactive document.\n",
    "# = 'png': Works for both (but figures can be grainy).\n",
    "%config InlineBackend.figure_format = 'svg'\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "A lesson of the last chapter is that *data does not speak for itself*. Thus, if we have empirical data (observational or experiments), we need a tool to investigate and present the data. Historically this was done by pen-and-paper calculations and hand-drawn figures. Today, computers are used to examine data. \n",
    "\n",
    "Computers think in terms of ones and zeros, and this way of thinking is very difficult for humans. Thus \\emph{computer languages} have been invented as a compromise between the way computers and humans think. These languages typically look like written English -- to some extent. Unlike spoken language, the syntax of a computer language is rigorous and does not allow for vague statements. It can be a frustrating experience the first time you learn a computer language. As for any language, you need to use it to learn it. Do not try to memorize all the syntax all at once, but discover it when you need it. Similarly, it is boring to learn Swahili by memorizing a dictionary. It is much more interesting to learn Swahili by moving to Africa.\n",
    "\n",
    "We will use Jupyter Notebook and Python (we introduced you to this in an exercise). There are many great tools we could also have used. What you learn in this course, you will be able to transfer to these tools. An advantage of Python is that it is relatively easy for newcomers, while it widely used by scientists. Later in this course you will learn about modules for more advanced tasks. This chapter, however, gives a basic introduction to Python and Jupyter notebooks. To release the power of Python, we need to know some of it's quirks (see Table \\ref{table_python_basics} page \\pageref{table_python_basics}).\n",
    "\n",
    "\\input{table_python_basics.tex}\n",
    "\n",
    "In the following, we will assume that you have completed the exercise of the first chapter where you open a Jupyter Notebook and entered some text into a cell. Below we will explore some more Python code you can enter into cells. It is recommended that you open a Jupyter Notebook and type in the examples of the text.\n",
    "\n",
    "\\begin{exercise}{A peculiar friend}\n",
    "Imagine you have a peculiar friend who thinks like a computer. You enter an restroom with the sign \"Only toilet paper in the bowl!\". The sign is to avoid that the toilet get clogged when flushing.\n",
    "\\begin{itemize}\n",
    "    \\item Discuss why your peculiar friend cannot use this restroom, and why you (probably) would use the it without hesitation. \n",
    "\\end{itemize}\n",
    "The take home message is that it can be frustating to write computer code since computers cannot infere context as Humans can.\n",
    "\\end{exercise}\n",
    "\n",
    "# Numerical data\n",
    "\n",
    "## Integers and floats\n",
    "The cells in Jupyter Notebook can be used for simple calculations. You can type an simple expression, and it will output the value of the last line of the cell. The syntax for a simple expression is straightforward: the **operators**\\index{operators} \\verb|+|, \\verb|-|, \\verb|*| and \\verb|/| work just as you would expect. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "5"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "2 + 3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "-1"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "2 - 3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "6"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "2 * 3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.6666666666666666"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "2 / 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Parentheses (\\verb|()|) can be used for grouping statements."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "9.5"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(49 - 5*6) / 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Double asterisk (\\verb|**|) is used raise a number to a power. Here we have two to the power of three ($2^3=2\\cdot2\\cdot2=8$)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "8"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "2**3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Taking the square root of a number is the same as rasing to the power $\\frac{1}{2}$: $$\\sqrt{y}=y^{0.5}$$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1.4142135623730951"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "2**0.5  # Square root of two"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Above the hashtag (\\verb|#|) is use to write comments to Humans (not read by the computer)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that the outputs of the above calculations result in different data types. The first is an integer (5) while the last is a decimal number (1.4142135623730951). The **integer numbers**\\index{integer} (e.g. 2, 4, 20; *DK: heltal*) have type \\verb|int| in Python. The decimal number, i.e. the ones with a fractional part (e.g. 5.0, 1.6), are also called **floating point numbers**\\index{float}\\index{floating point number} (*DK: decimaltal*) and have type \\verb|float|. Note that a dot \\verb|.| is used for the decimal separator (*DK: på Dansk bruges \\verb|,|!*)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Briefly about build-in functions\n",
    "Python has several built-in functions that we will introduce as we need them. The built-in function \\verb|type()| can be used to show the type of a variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "int"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "type(4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "float"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "type(9.5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The result of a calculation (or content of a variable) can be viewed using the built-in function \\verb|print()| or the Jupyter Notebook \\verb|display()| (the latter is sometimes more pretty but is not native to Python). The value is always displayed if it is on the last line of a Jupyter Notebook cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1.0\n"
     ]
    }
   ],
   "source": [
    "display(2 + 2)\n",
    "print(2 / 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Type casting (converting from one type to another)\n",
    "\n",
    "If you want to enforce that a variable is of a given type you can use the built-in function \\verb|int()| or \\verb|float()|."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "int(3.14159)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3.0"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "float(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In addition to \\verb|int| and \\verb|float|, Python supports other types of numbers, such as \\verb|Decimal| and \\verb|Fraction|. Python also has built-in support for complex numbers and uses the \\verb|j| or \\verb|J| suffix to indicate the imaginary part (e.g. \\verb|3+5j|). But in this course, we will only use \\verb|int| and \\verb|float| for representing numerical values. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Large numbers\n",
    "It is convinient to write large numbers like one thousand (1000) as $10^3$ or one million (1 000 000) as $10^3$. Python use \\verb|e| for this \\emph{scientific notation}. As an example, if we want to write $3200=3.2\\times10^3$ we type"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3200.0"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "3.2e3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that Python return a \\verb|float| .You can use \\verb|int()| if you really want an integer."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\newpage\n",
    "\\begin{exercise}{Working with numbers using Python}\n",
    "\tOpen a Jupyter notebook and compute the following. Confirm that you get the same with a pen and paper (you did this in an earlier exercise):\n",
    "\t\\begin{eqnarray}\n",
    "\t\t3.119 + 1.211 &=& \\\\\n",
    "\t\t7354 + 1246 &=& \\\\\n",
    "\t\t78+14/3 &=& \\\\\n",
    "\t\t(78+14)/3 &=& \\\\\n",
    "\t\t1+2\\times(3+2) &=& \\\\\n",
    "\t\t\\frac{2+4}{2} &=& \\\\\n",
    "\t\t\\frac{1}{5}\\left(4+2+3+2+4\\right) &=& \\\\ [10pt]\n",
    "\t\t\\frac{2}{\\frac{1}{3}} &=& \\\\ [10pt]\n",
    "\t\t\\frac{\\frac{2}{3}}{\\frac{3}{5}} &=& \\\\ [10pt]\n",
    "\t\t\\frac{1}{2} + \\frac{5}{6} &=& \\\\\n",
    "\t\t2^2 &=& \\\\\n",
    "\t\t\\sqrt{4^2} &=& \\\\\n",
    "\t\t2^2+4^2 &=& \\\\\n",
    "\t\t4^3 + 3^2 &=& \\\\\n",
    "\t\t\\sqrt{16} &=& \\\\\n",
    "\t\t\\frac{2\\times10^3+4\\times10^2}{2\\times10^2} &=& \\\\\n",
    "\t\t\\frac{2\\times10^{23}+4\\times10^{22}}{2\\times10^{22}} &=&\t\n",
    "\t\\end{eqnarray}\n",
    "\\end{exercise}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Assign number to memory using variables \n",
    "It is often convenient to assign data to a variable with a descriptive name. The data is then stored in the computer's memory, and we can access it by referring to that name. In Python, the equal sign (\\verb|=|) assigns a value to a variable. Below is an example where we assign two integers to variables and then use the variables to make a simple calculation of an area."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "12"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "width = 3\n",
    "height = 4\n",
    "width * height"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To make the code easy to read, you should pick names that describe the data -- such as \\verb|width| and \\verb|height| in the above example. It is good practice to use descriptive names so that the code is self-explanatory. We recommend using an underscore \\verb|_| to separate the words for names that consist of several words -- you are not allowed to use space.\n",
    "\n",
    "Note that the \\verb|=| above is fundamentally different from the $=$ in a mathematical sense. For the mathematical $=$ sign, the left-hand side and the right-hand side are equal. What is left of the \\verb|=| sign above (here \\verb|width| and \\verb|height|) represents a location in the computer's physical memory. What is right of the \\verb|=| sign (\\verb|3| and \\verb|4|) represents the data assigned to that location. Thus, you will get a syntax error if you switch the two sides. If a variable is not “defined” (assigned a value), trying to use it will give you an error."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{exercise}{Calculations with variables}\n",
    "\\begin{enumerate}\n",
    "\\item If \\verb|a=1| and \\verb|b=2| what is then \\verb|a+b|. Type the code into a Jupyter cell and confirm that you get what you expected.\n",
    "\\item Imagine that the value \\verb|100| is stored in the variable \\verb|x|.\n",
    "\\begin{enumerate}\n",
    "  \\item What happens to the value when \\verb|x=x+1| is executed? First write down your expectations on a piece of paper, and then confirm your answer by executing code. \n",
    "  \\item What is the solution to the mathematical equation $$x=x+1$$ Hint: Sometimes an algebraic equation does not have any solutions. Is that the case here?\n",
    "  \\end{enumerate}\n",
    "\\item Imagine you wished to compute two thirds of the circle constant known as $\\pi$: $$\\frac{2}{3}\\pi$$\n",
    "\\begin{enumerate}\n",
    "  \\item Write Python code where you first defines the floating-point variables \\verb|pi=3.14159| and \\verb|fraction=2/3|.\n",
    "  \\item Then multiply the variables and print the result. \n",
    "  \\item If your code is correct, you should get something like \\verb|2.09439333333333|. The true answer to the question is closer to \\verb|2.0943951023931953|. Why did we get a different answer?\n",
    "  \\item Would this \\emph{truncation error} be important if the calculation should be compared to empirical measurements where the numbers correspond to centimeters measured by a ruler?\n",
    "  \\end{enumerate}\n",
    "\\end{enumerate}\n",
    "\\end{exercise}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Compound data types\n",
    "Above, we stored a single numerical value for each variable name. Python knows several **compound data types**\\index{compound data type}\\index{data type!compound}, used to group together several values. This is handy for storing data.\n",
    "\n",
    "## Text string\n",
    "Not all data is numerical (think about categorical variables). Besides numbers, computers can also represent data as characters: \\verb|a|, \\verb|b|, \\verb|c| and so on. A sequence of characters are known as **strings**\\index{string}\\index{text string}, i.e. bites of text. We can express this data type using single quotes ('...') or double quotes (\"...\")."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'eggs'"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "'eggs'  # a string"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Formatted text strings"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use strings to present results to Humans in an appealing style. If you want to include the value of a variable, you can use a **formatted string**\\index{formatted string}\\index{string!formatted} by adding an \\verb|f| before the first quote. The name of the variable is then put inside curly brackets  \\verb|{}| (*DK: tuborgklammer*)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'The width is 3'"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "f'The width is {width}'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Long strings can span multiple lines when using three quotes. Below is an advanced example using a string spanning multiple lines, and what we have learned above (notice that a calculation can also be included in the curly brackets)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "If the width is 3\n",
      "and the height is 4\n",
      "then the area is 12.\n"
     ]
    }
   ],
   "source": [
    "print(f'''\n",
    "If the width is {width}\n",
    "and the height is {height}\n",
    "then the area is {width*height}.''')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{exercise}{Hello World!}\n",
    "Print \\verb|Hello World!| using a Python string and the build-in \\verb|print()| function.\n",
    "\\end{exercise}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{exercise}{The area of a football field}\n",
    "\n",
    "A football field has a width of 68 meters and a length of 105 meters. Correct the code block below, so the area of the field is computed (the answer should be: \"The area of a football field is 7140 square meters\"). There are several mistakes in the code. For this exercise, you should correct one error at a time and inspect error messages.\n",
    "\n",
    "\\pycode{football_field.py}\n",
    "\n",
    "When you have corrected the above code, the variable \\verb|area| could be of the type \\verb|int| (integer). Confirm this by using the built-in function \\verb|type()|. Area is a continuous variable, thus it should really be a \\verb|float|. Why is that, and how can you correct the code to ensure that it really is a \\verb|float| (assuming that you have not already done so)?\n",
    "\n",
    "\\end{exercise}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## List\n",
    "A versatile compound datatype is a **list**\\index{list} of data. In Python it is written as comma-separated values (items) between square brackets (\\verb|[]|). Lists might contain items of different types (e.g. integers, floats and strings),"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0, 4.686, 'My mothers hat']"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "[0, 4.686, 'My mothers hat']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Usually all the items have the same type. Below we create a list of integers and store the data as a variable named \\verb|squares|)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0, 1, 4, 9, 16, 25]"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "squares = [0, 1, 4, 9, 16, 25]\n",
    "squares"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first element has the index 0, and can be accessed by using square brackets (\\verb|[]|)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "squares[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Other elements can be accessed in the same way. Since we start counting at zero, the 4th item is achieved by using index `3`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "9"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "squares[3]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A list can created by slicing another list by using a colon (`:`). Below we create a new list starting from the 2nd element to 4th."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[1, 4, 9]"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "squares[1:4]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Some methods for lists\n",
    "The list data type has some **methods**\\index{method} attached to it. As an example, the method \\verb|append()| adds an item to the end of the list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[2, 4, 6]"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "integers = [2, 4]\n",
    "integers.append(6)\n",
    "integers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can read the documentation for the method by using the \\verb|help()| function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Help on built-in function append:\n",
      "\n",
      "append(object, /) method of builtins.list instance\n",
      "    Append object to the end of the list.\n",
      "\n"
     ]
    }
   ],
   "source": [
    "help(integers.append)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can get the length of a list using the built-in function \\verb|len()|"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3"
      ]
     },
     "execution_count": 26,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "len(integers)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can get the sum of the elements using the built-in function \\verb|sum()|"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "12"
      ]
     },
     "execution_count": 27,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "sum(integers)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The `*` operator on list\n",
    "The `*` operator can be used to repeate a list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[5, 2, 5, 2, 5, 2]"
      ]
     },
     "execution_count": 28,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "[5, 2]*3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "\\begin{exercise}{Compute the mean using a list}\n",
    "\n",
    "If we have a list of numerical data, we can compute the average using the built-in \\verb|len()| and \\verb|sum()| functions. Later we will learn that the mean is computed by adding all the values and then dividing with the number of values.\n",
    "\n",
    "\\begin{enumerate}\n",
    "\\item Use pen and paper to compute the mean of the values four, two and six.\n",
    "\\item Make a list containing the integer numbers four, two and six. \n",
    "\\item. Use the built-in function \\verb|sum()| and \\verb|len()| to compute the mean.\n",
    "\\end{enumerate}\n",
    "\n",
    "\\end{exercise}\n",
    "\n",
    "\\begin{exercise}{Add lots of numbres}\n",
    "\n",
    "In this exercise we will us the computer to evaluate\n",
    "$$\n",
    " \\sum_{k=0}^{100} k\n",
    "$$\n",
    "The build-in function \\verb|range()| can be used to create a range of integers. As an example, if we want a list of the integers from 0 to 100 we can write \\verb|list(range(101))|. \n",
    "\n",
    "\\begin{enumerate}\n",
    "\\item Create a list containing the first 101 integers (including zero).\n",
    "\\item Compute the sum of the integers in the list using the build-in \\verb|sum()| function.\n",
    "\\end{enumerate}\n",
    "\n",
    "\\end{exercise}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The tuple\n",
    "Later in the compendium I will sometimes use a Tuples. They are very similar to lists with a minor tecnical differences (they are \\emph{immutable}). They are defined using soft parentheses. Here is an example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(4, 3)"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(4, 3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dictionary\n",
    "\n",
    "Another useful data type built into Python is the **dictionary**\\index{dictionary}. Unlike lists, which are indexed by a range of integers, dictionaries are indexed by keys that we can choose. Strings, tuples or numbers can be keys. However, you can NOT use lists as keys (this is the abovementioed \\emph{tecnical differences} -- the key needs to be an \\emph{immutable} data type).\n",
    "\n",
    "Think of a dictionary as a set of \\verb|key: value| pairs, with the requirement that the keys are unique (within one dictionary). Place comma-separated (\\verb|,|) \\verb|key:value| pairs within braces (\\verb|{}|).\n",
    "\n",
    "The main operations on a dictionary are storing a value with some key and extracting the value given the key. If you store using a key that is already in use, the old value associated with that key is forgotten. It is an error to extract a value using a non-existent key.\n",
    "\n",
    "Here is an example using a dictionary where composer names are used as keys, and the values are their birth years."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'Wolfgang Mozart': 1756,\n",
       " 'Ludwig van Beethoven': 1770,\n",
       " 'Johann Sebastian Bach': 1685}"
      ]
     },
     "execution_count": 30,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "birth_year = {\n",
    "    'Wolfgang Mozart': 1756,\n",
    "    'Ludwig van Beethoven': 1770,\n",
    "    'Johann Sebastian Bach': 1685\n",
    "}\n",
    "birth_year"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A new entry in a dictonary can be made like this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [],
   "source": [
    "birth_year['Justin Bieber'] = 1994"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An entry can be fetched by writing the key inside \\verb|[ ]|."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1770"
      ]
     },
     "execution_count": 32,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "birth_year['Ludwig van Beethoven']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{exercise}{Make a dictionary of flower data}\n",
    "\n",
    "\\begin{enumerate}\n",
    "\\item Make a dictionary named `poinsettias` with the Poinsettias data in Table \\ref{table_poinsettias} (page \\pageref{table_poinsettias}) where colors are used as keys.\n",
    "\\item Demonstrate that you get the value `108` if you type in the key `Red`.\n",
    "\\end{enumerate}\n",
    "\n",
    "Later you will use this dictionary to make a so-called Pandas DataFrame, but more about this in the next chapter.\n",
    "\n",
    "\\end{exercise}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Booleans\n",
    "Consider a binary variable, i.e. a categorical variable that can only be one of two values such as true/false, on/off, yes/no or dead/alive. In programming languages, such a variable is referred to as a **boolean**\\index{boolean variable}\\index{variable!boolean}, and it has a value of either \\verb|True| or \\verb|False|."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 33,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "to_be_or_not_to_be = False\n",
    "to_be_or_not_to_be"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Booleans are often used to compare values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 34,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "a = 5\n",
    "a < 10"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The standard **comparison operators**\\index{comparison operators}\\index{operators!comparison} are: \\verb|<| (less than), \\verb|>| (greater than), \\verb|==| (equal to), \\verb|<=| (less than or equal to), \\verb|>=| (greater than or equal to) and \\verb|!=| (not equal to). Notice that this introduces a third kind of equal sign, \\verb|==|, not to be confused with the one we used above to assign a value to a location in memory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 35,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "a = 5\n",
    "a == 10"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "\\begin{exercise}{Variable types}\n",
    "\n",
    "\n",
    "Consider the below list of variables described in Section \\ref{types_of_variables} (page \\pageref{types_of_variables}). Pick an example of such a variable, and discuss with your neighbor how you would represent these types of variables using Python.\n",
    "\n",
    "\\begin{enumerate}\n",
    "\\item A Binary variable.\n",
    "\\item A categorical variable.\n",
    "\\item A discrete variable.\n",
    "\\item A continuous numerical variable.\n",
    "\\end{enumerate}\n",
    "\n",
    "\\end{exercise}\n",
    "\n",
    "\n",
    "\\begin{exercise}{Coding questions}\n",
    "\n",
    "\\begin{enumerate}\n",
    "\\item If \\verb|n = 73|, \\verb|f = 7.3|, \\verb|l = [7,3]|, \\verb|d={'dwarfs':7, 'pigs':3}| and \\verb|b = 7==3| what are then the data types of the variables \\verb|n|, \\verb|f|, \\verb|l|, \\verb|d| and \\verb|b|? First write down your answers, and then check them by using the built-in \\verb|type()| function.\n",
    "\\item What Boolean value (\\verb|True| or \\verb|False|) is returned when the operator \\verb|>| is used between the numbers \\verb|1.6| and \\verb|2.3|. First write down your expectations on a piece of paper, and then confirm your answer by executing code. \n",
    "\\end{enumerate}\n",
    "\n",
    "\\end{exercise}\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
