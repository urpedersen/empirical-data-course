# Import Pandas and Matplotlib
import pandas as pd
import matplotlib.pyplot as plt

# Store data in Pandas DataFrame
df = pd.DataFrame({
    'yield': [
        241, 230, 207, 219, 266, 167,
        204, 144, 178, 158, 153,
        187, 181, 196, 149, 183,
    ]
})

# Plot histogram
df['yield'].plot.hist(bins=10)
plt.xlabel('Yield of hybrid corn (bushels per acre)')
plt.show()
