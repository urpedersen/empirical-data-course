# Get started using Jupyter Notebook and Python

The central purpose of this course is to give a general understanding of how to examine empirical data.
For this, we will use [Jupyter Notebook](jupyter.org) and the [Python](www.python.org) computer language.

## Online-cloud services
[Jupyter Notebooks](jupyter.org) are available thought various online-cloud service 
that allows you to start using Jupyter Notebooks without making installations
on your computer (but you will have to agree to the providers terms and conditions):

* [Google Colab](https://colab.research.google.com) [Recomended]
* [CoCalc](https://cocalc.com/)
* [Microsoft Azure Notebooks](https://notebooks.azure.com/)
* [myBinder.org](https://myBinder.org/): Try to use the link to [this repository](https://mybinder.org/v2/gl/natural-sciences-ruc%2Fempirical-data-course/master)
 (gitlab.com/natural-sciences-ruc/empirical-data-course)

## Install the Anaconda package on your computer
You can get everything you need to your computer with the 
[Anaconda](https://www.anaconda.com/) distribution. Go to the download page 
(https://www.anaconda.com/distribution/) and install the newest version on your
computer*. After the installation, you should launch Jupyter, and you are ready for the exercises.

If you are on a Linux machine, it is recommended to use the package manager.

